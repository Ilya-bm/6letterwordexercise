﻿using System.Collections.Generic;

namespace LetterWord.Bl.Model 
{
  public class WordCombinationInput 
  {
    public List<string> Words { get; set; }
    public Dictionary<string, int> CombinationDictionary { get; set; }
  }
}
