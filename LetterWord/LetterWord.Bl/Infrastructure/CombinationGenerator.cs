﻿using System;
using System.Collections.Generic;
using System.Linq;
using LetterWord.Bl.Extensions;
using LetterWord.Bl.Model;

namespace LetterWord.Bl.Infrastructure {
  public class CombinationGenerator : ICombinationGenerator {
    /// <summary>
    /// find combinations for pre-defined string length based on word combination.
    /// </summary>
    /// <param name="input"></param>
    /// <param name="combinationLength"></param>
    /// <returns></returns>
    public IList<string> Generate(WordCombinationInput input, int combinationLength = 6) {
      if (input == null) {
        throw new ArgumentNullException(nameof(input));
      }

      if (input.CombinationDictionary == null || input.Words == null) {
        return null;
      }

      var combinationResults = new List<string>();
      var possibleCombinations = new List<List<int>>();

      GetStringCombinationIndexes(combinationLength,
        0,
        0,
        new int?[combinationLength * 2 - 1],
        possibleCombinations);

      foreach (var word in input.Words) {
        if (word.Length != combinationLength) {
          throw new Exception($"word length should be equal to {combinationLength}");
        }
        foreach (var possibleCombination in possibleCombinations) {
          var splittedDest = word.SplitAt(possibleCombination);
          if (word.SplitAt(possibleCombination).GroupBy(s => s).All(g => {
            if (input.CombinationDictionary.TryGetValue(g.Key, out int v) && v >= g.Count()) {
              return true;
            }
            return false;
          })) {
            combinationResults.Add($"{string.Join("+", splittedDest)}={word}");
          }
        }
      }

      return combinationResults;
    }

    /// <summary>
    /// set possible substring combinations of defined string length . eg : length 5. apple - app le [2]. ap p le [1,2]
    /// </summary>
    /// <param name="stringLength"></param>
    /// <param name="spaceStringLength"></param>
    /// <param name="destLength">length of processing string for combination</param>
    /// <param name="wordCombination">contain combination of word. null - char value, 1 - separator. </param>
    /// <param name="stringCombinationIndexes"></param>
    /// <param name="endWithSpace"></param>
    private static void GetStringCombinationIndexes(
      int stringLength,
      int spaceStringLength,
      int destLength,
      int?[] wordCombination,
      List<List<int>> stringCombinationIndexes,
      bool endWithSpace = false) {
      if (stringLength == destLength) {
        int charPosition = 0;
        var stringSeparatorCombination = new List<int>();
        for (var i = 0; i < wordCombination.Length; i++) {
          if (wordCombination[i] == null) {
            if (i != 0) {
              charPosition++;
            }
          }
          else {
            stringSeparatorCombination.Add(charPosition + 1);
          }
        }

        if (stringSeparatorCombination.Any()) {
          stringCombinationIndexes.Add(stringSeparatorCombination);
        }
      }
      else {
        if (spaceStringLength == 0 || endWithSpace) {
          // set equality of symbol.
          wordCombination[spaceStringLength] = null;
          GetStringCombinationIndexes(stringLength, spaceStringLength + 1, destLength + 1, wordCombination, stringCombinationIndexes);
        }
        else {
          // set space
          wordCombination[spaceStringLength] = 1;
          GetStringCombinationIndexes(stringLength, spaceStringLength + 1, destLength, wordCombination, stringCombinationIndexes, true);
          // set equality of symbol. 
          wordCombination[spaceStringLength] = null;
          GetStringCombinationIndexes(stringLength, spaceStringLength + 1, destLength + 1, wordCombination, stringCombinationIndexes);
        }
      }
    }
  }

  public interface ICombinationGenerator {
    IList<string> Generate(WordCombinationInput input, int combinationLength = 6);
  }
}
