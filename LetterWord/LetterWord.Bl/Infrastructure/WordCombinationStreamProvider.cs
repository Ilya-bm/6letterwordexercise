﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LetterWord.Bl.Model;

namespace LetterWord.Bl.Infrastructure {
  public class WordCombinationStreamProvider : IWordCombinationStreamProvider {
    public async Task<WordCombinationInput> Read(StreamReader inputStream, int combinationLength = 6) {
      var result = new WordCombinationInput() {
        Words = new List<string>(),
        CombinationDictionary = new Dictionary<string, int>()
      };

      var hsWord = new HashSet<string>();
      string line;
      while ((line = await inputStream.ReadLineAsync()) != null) {
        if (line.Length > combinationLength || line.Length == 0) {
          continue;
        }

        if (line.Length == combinationLength) {
          hsWord.Add(line);
        }
        else {
          if (result.CombinationDictionary.ContainsKey(line)) {
            result.CombinationDictionary[line]++;
          }
          else {
            result.CombinationDictionary.Add(line, 1);
          }
        }
      }

      result.Words = hsWord.ToList();
      return result;
    }
  }

  public interface IWordCombinationStreamProvider {
    Task<WordCombinationInput> Read(StreamReader inputStream, int combinationLength = 6);
  }
}
