﻿using System.Collections.Generic;

namespace LetterWord.Bl.Extensions {
  public static class StringExtensions {
    public static string[] SplitAt(this string source, List<int> index) {
      var output = new string[index.Count + 1];
      int pos = 0;
      for (int i = 0; i < index.Count; pos = index[i++]) {
        output[i] = source.Substring(pos, index[i] - pos);
      }

      output[index.Count] = source.Substring(pos);
      return output;
    }
  }
}

