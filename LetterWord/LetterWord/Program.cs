﻿using System;
using System.IO;
using LetterWord.Bl.Infrastructure;
using LetterWord.Bl.Model;

namespace LetterWord.ConsoleApp {
  public class Program {
    static void Main(string[] args) {
      WordCombinationInput combinationInput;
      using (var streamReader = new StreamReader("input.txt")) {
        var reader = new WordCombinationStreamProvider();
        combinationInput = reader.Read(streamReader).GetAwaiter().GetResult();
      }

      var combinations = new CombinationGenerator().Generate(combinationInput);
      if (combinations != null) {
        foreach (var combination in combinations) {
          Console.WriteLine(combination);
        }
      }

      Console.ReadKey();
    }
  }
}
